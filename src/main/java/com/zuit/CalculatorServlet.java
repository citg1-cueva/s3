package com.zuit;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7009488492835154690L;
	//initialized 
	public void init() throws ServletException{
		System.out.println("*************");
		System.out.println("Initialized Connection to the Database.");
		System.out.println("*************");
	}
	//post
	public void  doPost(HttpServletRequest req, HttpServletResponse res ) throws IOException {
		System.out.println("Hello from the caculator servlet");
		System.out.println("Hello from the calculator servlet.");
		
		String operations = req.getParameter("op");
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		int ans = 0;
		String sign = "";
		
		PrintWriter out = res.getWriter();
		if(operations.charAt(0) == '+') {
			ans = num1 + num2;
			sign = "add";
			System.out.println("*******"+operations+"");
		}
		else if(operations.charAt(0) == '-') {
			ans = num1 - num2;
			sign = "subtract";
			System.out.println("*******"+operations+"");
		}
		else if(operations.charAt(0) == '*') {
			ans = num1 * num2;
			sign = "multiply";
			System.out.println("*******"+operations+"");
		}
		else {
			ans = num1 / num2;
			sign = "devide";
		}
		
		out.println("The number are: "+num1+","+num2);
		out.println("The operation that you wanted "+sign);
		out.println("The Result:"+ ans);
	}
	//get
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You are using the Calculator App</h1>");
		out.println("<h4>To use the App, Enter two number and an operations</h4>");
		out.println("<h4>Hit the submit filling in the details</h4>");
		out.println("<h4>You will get your reasult on the screen!</h4>");
		
	}
	//finalization 
	public void destroy() {
		System.out.println("*************");
		System.out.println("Disconnected from Database.");
		System.out.println("*************");
	}
	

}
